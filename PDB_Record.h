#ifndef PDB_RECORD 
#define PDB_RECORD

#include <QString>
#include <QList>
#include <PDB_Property.h>
#include <PDB_PropertyType.h>
class PDB_Record
{
private:
	QString PDB_ID;
	QList<PDB_Property> *pdbPropertyList;
	QString PDB_Description;

	QString PDB_Name;
	QString PDB_Method;
	QString PDB_Authors;
public:
	PDB_Record();
	virtual ~PDB_Record();

	//���������� �������� � ������ ������� ������
	bool addpdbProperty(const PDB_PropertyType &pdbPropertyType, const QString &pdbPropertyName, const QString &pdbPropertyValue);
	void addpdbProperty(const PDB_Property &pdbProperty);
	//
	PDB_Property getpdbProperty(const unsigned int &number) const;
	int pdbPropertyListCount() const;

	//PDB_Description;
	const QString& get_PDB_Description() const;
	bool set_PDB_Description(const QString &Value);
	//PDB_ID
	const QString& get_PDB_ID() const;
	bool set_PDB_ID(const QString &Value, const QList<QString> &pdbRecordsList);
	bool set_PDB_ID(const QString &Value);
};

#endif //PDB_RECORD