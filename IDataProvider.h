#ifndef IDATAPROVIDER 
#define IDATAPROVIDER

#include <QString>
#include <PDB_Record.h>
/*
����������� ����� IDataProvider �������� ������� ��� ���� DataProvider-��.
����� ������� � ����������� ��� ������ ������ ���� ��������������
������� ��� � ������� �� �������, ��� �� ����� ����������� ���������� � ����� ������
*/
class IDataProvider
{
public:
	//virtual ~IDataProvider() = 0;

	//������� ��������� ������
	virtual bool DefineDataSource(const QString &sourceFileName)=0;
	//�������� ������ �� ��������� ���������
	virtual bool LoadDataBase()=0;
	//�������� ������ ��������� ������
	virtual bool CreateNewDataBase(const QString &newFileName) = 0;
	//���������� ������ � �������� �������� ������
	virtual bool SaveDataBase()=0;
	//��������� ��������� ������ � ��������� ������
	virtual bool SaveSelectedRecord(const QString &oldPDB_ID, const PDB_Record &record)=0;
	//�������� ��������� ������ �� ��������� ������
	virtual bool DeleteSelectedRecord(const QString &PDB_ID) = 0;
	//���������� ��������� ������ � �������� ������
	virtual bool AddRecord(const PDB_Record &record)=0;
	//������� ������ ��������������� ���� ��������� � ��������� ������ �������
	virtual QList<QString> GetRecordsList()=0; 
	//������� ������ �������� � ��������� ������, ��������������� ��������� ��������
	virtual PDB_Record GetSelectedRecord(const QString &nodeId) const =0;
};

#endif //IDATAPROVIDER
//IDataProvider::~IDataProvider()
//{
//
//}

