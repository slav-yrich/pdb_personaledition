#ifndef PROPERTY_TYPE 
#define PROPERTY_TYPE
enum PDB_PropertyType
{
	Text = 0,
	IntegerNumber = 1,
	RealNumber = 2,
	Picture = 3,
	WebLink = 4,
	File = 5
};
#endif //PROPERTY_TYPE
