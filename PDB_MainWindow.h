#ifndef  PDB_MAINWINDOW
#define PDB_MAINWINDOW

#include <ui_PDB_MainWindow.h>
#include <QMainWindow>
#include <QtXml>//-
#include <QFileDialog>
#include <QMessageBox>
#include <PDB_DataProvider_XML.h>
#include <PDB_PropertyType.h>
#include <QString>
#include <QList>
#include <QFileInfo>

class PDB_MainWindow: public QMainWindow, public Ui::PDB_MainWindow
{
	Q_OBJECT
private:
	PDB_DataProvider_XML *dataProvider;
	const QString PDB_DataSource_FileName= "PDB_DataSource.xml";
	int RhetoricalQuestionCounter = 0; //��������� ��������, ��� �� ������ � ����������� �������� �� ���� ������� �����������

	void SetReadOnlyEditFields(const bool &value); //����� �������� ReadOnly ������ value ���� ������������� �����
	void SetValidatorsFields(); //����� �������� ����������� � ��������� � �������� �����
	void ClearEditFields(); //������� ��� ���� �����
	//������ ������� �������� 
	void SetDefaultPicture();
	bool SetPicture(const QString &picturePath);

	PDB_Record AllFieldsToRecord(bool &check) const; //����� ���������� ��� ���� � ���� ������ ���� PDB_Record, �������� �������� �����
public:

	PDB_MainWindow(QWidget *parrentQWidget=0);
	virtual ~PDB_MainWindow();
	void ReadPDBRecordsList(); 

//slots:
private slots:
void slotShowSelectedRecord(QListWidgetItem *chooseItem);
void slotSaveSelectedRecord();
void slotAddRecord();
void slotDeleteRecord();
void slotLoadDataBase();
void slotEditRecordPicture();

void slotCreateNewRecordMode(); //�������������� ���� ��� �������� ����� ������
void slotEditMode(); //�������������� ���� ��� �������������� ��������� ������
void slotInitialStateMode();//��������������� ��������� ��������� ����

//��� ��� � plainTextEdit � ���������� �� �����, �� ��������� ���� ��������� 
//�������� �������� � ������������� � ����������� ����� ������ ���� plainTextEdit
void slotCheckShortDescription(); 


};

#endif //PDB_MAINWINDOW