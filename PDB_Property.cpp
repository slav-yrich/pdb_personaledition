#include <PDB_Property.h>

PDB_Property::PDB_Property(PDB_PropertyType value)
{
	pdbPropertyType= value;
	pdbPropertyName="";
	pdbPropertyValue="";
}

bool PDB_Property::CheckValue(const PDB_PropertyType& pdbPropertyTypeCheckValue, const QString& value) const
{
	bool resultCheck;
	try
	{
		switch (pdbPropertyTypeCheckValue)
		{
		case Text:
			resultCheck = CheckText(value);
			break;
		case IntegerNumber:
			value.toInt(&resultCheck);
			break;
		case RealNumber:
			value.toDouble(&resultCheck);
			break;
		case Picture:
			resultCheck = (QPixmap()).load(value);
			break;
		case WebLink:
			resultCheck = CheckURL(value);
			break;
		case File:
			resultCheck = QFile::exists(value);
			break;
		}
	}
	catch (const std::exception& exc)
	{
		resultCheck = false;
		qDebug() << "CheckValue error:"<<exc.what();
	}
	return resultCheck;
}

bool PDB_Property::CheckText(const QString& value)
{
	value.trimmed();
	return GetQRegExpForText().exactMatch(value);
}

bool PDB_Property::CheckURL(const QString& value)
{
	value.trimmed();
	return GetQRegExpForURL().exactMatch(value);
}

QRegExp PDB_Property::GetQRegExpForText()
{
	return QRegExp("^[a-zA-Z0-9\\s.,;:!?\"\'()]+$");
}

QRegExp PDB_Property::GetQRegExpForURL()
{
	return QRegExp("^(?:(?:https?|ftp|telnet)://(?:[a-z0-9_-]{1,32}(?::[a-z0-9_-]{1,32})?@)?)?(?:(?:[a-z0-9-]{1,128}\.)+(?:ru|su|com|net|org|mil|edu|arpa|gov|biz|info|aero|inc|name|[a-z]{2})|(?!0)(?:(?!0[^.]|255)[0-9]{1,3}\.){3}(?!0|255)[0-9]{1,3})(?:/[a-z0-9.,_@%&?+=\~/-]*)?(?:#[^ '\"&]*)?$~i");
}

QString PDB_Property::pdbPropertyToQString(const PDB_PropertyType& pdbProperty)
{
	QString result;
	switch (pdbProperty)
	{
	case Text:
		result = "Text";
		break;
	case IntegerNumber:
		result = "IntegerNumber";
		break;
	case RealNumber:
		result = "RealNumber";
		break;
	case Picture:
		result = "Picture";
		break;
	case WebLink:
		result = "WebLink";
		break;
	case File:
		result = "File";
		break;
	}
	return result;
}

PDB_PropertyType PDB_Property::QStringTopdbProperty(const QString &pdbPropertyStr)
{
	if (pdbPropertyStr == "Text")			return Text;
	if (pdbPropertyStr == "IntegerNumber")	return IntegerNumber;
	if (pdbPropertyStr == "RealNumber")		return RealNumber;
	if (pdbPropertyStr == "Picture")			return Picture;
	if (pdbPropertyStr == "WebLink")			return WebLink;
	if (pdbPropertyStr == "File")			return File;

	return Text; //Nado mozshet bit dobavit PDB_PropertyType::Unknow
}

const PDB_PropertyType& PDB_Property::GetpdbPropertyType() const
{
	return pdbPropertyType;
}
const QString& PDB_Property::GetpdbPropertyName() const
{
	return pdbPropertyName;
}
const QString& PDB_Property::GetpdbPropertyValue() const
{
	return pdbPropertyValue;
}


bool PDB_Property::SetpdbPropertyName(const QString& value)
{
	if (CheckText(value) && (value != ""))
	{
		pdbPropertyName = value;
		return true;
	}
	return false;
}

bool PDB_Property::SetpdbPropertyValue(const QString& value)
{
	if (CheckValue(pdbPropertyType, value))
	{
		pdbPropertyValue = value;
		return true;
	}
	return false;
}