TEMPLATE=	app
CONFIG +=	qt
QT +=		core widgets xml

FORMS =		PDB_MainWindow.ui

HEADERS	=	PDB_MainWindow.h \
			PDB_Record.h \
			IDataProvider.h \
			PDB_DataProvider_XML.h \
			PDB_Property.h \
			PDB_PropertyType.h	

SOURCES =	main.cpp\
			PDB_MainWindow.cpp \
			PDB_Record.cpp \
			PDB_DataProvider_XML.cpp \
			PDB_Property.cpp 
