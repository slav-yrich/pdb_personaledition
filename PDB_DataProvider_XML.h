#ifndef PDB_DATAPROVIDER_XML 
#define PDB_DATAPROVIDER_XML

#include <IDataProvider.h>
#include <PDB_Record.h>
#include <QtXml>
#include <QString>
#include <QList>
#include <QFile>

class PDB_DataProvider_XML:public IDataProvider
{
private:
	QDomDocument *domDoc; 
	QString fileName;

	//Поиск записи с указанными параметрами (по сути сейчас это поиск первого вхождения, если кто-то вручную добавит две записи с не уникальными id
	//Метод возвращает true в случае успешного поиска и записывает результат в selectedDomElement
	bool FindSelectedRecord(QDomElement &selectedDomElement, const QString &PDB_ID) const; //Стоит попробывать XQuery
	//Элемент создающий узел QDomElement
	QDomElement getNewElement(const QString &TagName,
		const QString &Attribute1_Name, const QString &Attribute1_Value,
		const QString &Attribute2_Name, const QString &Attribute2_Value,
		const QString &TextValue) const;
	//Метод создающий узел QDomElement с тегом Record на основе параметров записи PDB_Record
	QDomElement RecordToDomElement(const PDB_Record &record) const;
public:
	PDB_DataProvider_XML();
	virtual ~PDB_DataProvider_XML();

	bool DefineDataSource(const QString &sourceFileName) override;
	bool LoadDataBase() override;

	bool CreateNewDataBase(const QString &newFileName) override;
	bool SaveDataBase() override;

	bool SaveSelectedRecord(const QString &oldPDB_ID, const PDB_Record &record) override;
	bool DeleteSelectedRecord(const QString &PDB_ID) override;
	bool AddRecord(const PDB_Record &record) override;

	QList<QString> GetRecordsList() override;
	PDB_Record GetSelectedRecord(const QString &PDB_ID) const override;
};

#endif //PDB_DATAPROVIDER_XML
