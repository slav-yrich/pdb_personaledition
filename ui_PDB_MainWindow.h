/********************************************************************************
** Form generated from reading UI file 'PDB_MainWindow.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PDB_MAINWINDOW_H
#define UI_PDB_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_PDB_MainWindow
{
public:
    QAction *menuFile_Load;
    QWidget *centralwidget;
    QHBoxLayout *horizontalLayout_7;
    QVBoxLayout *verticalLayout_8;
    QListWidget *listWidget_RecordsList;
    QPushButton *pbtnCreateNew;
    QVBoxLayout *verticalLayout_7;
    QGroupBox *groupBox_InformationRecord;
    QVBoxLayout *verticalLayout_5;
    QHBoxLayout *horizontalLayout_3;
    QFrame *frame_ImageEditArea;
    QVBoxLayout *verticalLayout;
    QFrame *frame_ImageArea;
    QVBoxLayout *verticalLayout_9;
    QLabel *labelImage;
    QPushButton *pbtnImage;
    QSpacerItem *verticalSpacer;
    QVBoxLayout *verticalLayout_4;
    QLabel *label_PDBid;
    QLineEdit *lineEdit_PDBid;
    QGroupBox *groupBox_ShortDescriptionArea;
    QVBoxLayout *verticalLayout_3;
    QPlainTextEdit *plainTextEdit_ShortDescription;
    QGroupBox *groupBox_ProtertiesArea;
    QVBoxLayout *verticalLayout_6;
    QScrollArea *scrollArea_PropertiesArea;
    QWidget *scrollAreaWidgetContents;
    QVBoxLayout *verticalLayout_10;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_Name;
    QLineEdit *lineEdit_Name;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label_Method;
    QLineEdit *lineEdit_Method;
    QHBoxLayout *horizontalLayout_10;
    QLabel *label_Length;
    QSpinBox *spinBox_Length;
    QHBoxLayout *horizontalLayout_6;
    QLabel *label_Authors;
    QLineEdit *lineEdit_Authors;
    QHBoxLayout *horizontalLayout_11;
    QLabel *label_Resolution;
    QDoubleSpinBox *doubleSpinBox_Resolution;
    QHBoxLayout *horizontalLayout_9;
    QLabel *label_ModelCount;
    QSpinBox *spinBox_ModelCount;
    QSpacerItem *verticalSpacer_2;
    QFrame *frame_ButtonsArea;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout;
    QPushButton *pbtnDelete;
    QPushButton *pbtnAdd;
    QPushButton *pbtnEdit;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *pbtnCancel;
    QPushButton *pbtnSave;

    void setupUi(QMainWindow *PDB_MainWindow)
    {
        if (PDB_MainWindow->objectName().isEmpty())
            PDB_MainWindow->setObjectName(QStringLiteral("PDB_MainWindow"));
        PDB_MainWindow->resize(627, 713);
        menuFile_Load = new QAction(PDB_MainWindow);
        menuFile_Load->setObjectName(QStringLiteral("menuFile_Load"));
        centralwidget = new QWidget(PDB_MainWindow);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        horizontalLayout_7 = new QHBoxLayout(centralwidget);
        horizontalLayout_7->setObjectName(QStringLiteral("horizontalLayout_7"));
        verticalLayout_8 = new QVBoxLayout();
        verticalLayout_8->setObjectName(QStringLiteral("verticalLayout_8"));
        verticalLayout_8->setContentsMargins(-1, -1, -1, 5);
        listWidget_RecordsList = new QListWidget(centralwidget);
        listWidget_RecordsList->setObjectName(QStringLiteral("listWidget_RecordsList"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(listWidget_RecordsList->sizePolicy().hasHeightForWidth());
        listWidget_RecordsList->setSizePolicy(sizePolicy);
        listWidget_RecordsList->setMaximumSize(QSize(16777215, 16777215));

        verticalLayout_8->addWidget(listWidget_RecordsList);

        pbtnCreateNew = new QPushButton(centralwidget);
        pbtnCreateNew->setObjectName(QStringLiteral("pbtnCreateNew"));

        verticalLayout_8->addWidget(pbtnCreateNew);


        horizontalLayout_7->addLayout(verticalLayout_8);

        verticalLayout_7 = new QVBoxLayout();
        verticalLayout_7->setObjectName(QStringLiteral("verticalLayout_7"));
        groupBox_InformationRecord = new QGroupBox(centralwidget);
        groupBox_InformationRecord->setObjectName(QStringLiteral("groupBox_InformationRecord"));
        verticalLayout_5 = new QVBoxLayout(groupBox_InformationRecord);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        frame_ImageEditArea = new QFrame(groupBox_InformationRecord);
        frame_ImageEditArea->setObjectName(QStringLiteral("frame_ImageEditArea"));
        frame_ImageEditArea->setFrameShape(QFrame::NoFrame);
        frame_ImageEditArea->setFrameShadow(QFrame::Sunken);
        verticalLayout = new QVBoxLayout(frame_ImageEditArea);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        frame_ImageArea = new QFrame(frame_ImageEditArea);
        frame_ImageArea->setObjectName(QStringLiteral("frame_ImageArea"));
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(frame_ImageArea->sizePolicy().hasHeightForWidth());
        frame_ImageArea->setSizePolicy(sizePolicy1);
        frame_ImageArea->setMinimumSize(QSize(206, 206));
        frame_ImageArea->setMaximumSize(QSize(206, 206));
        frame_ImageArea->setFrameShape(QFrame::Box);
        frame_ImageArea->setFrameShadow(QFrame::Plain);
        verticalLayout_9 = new QVBoxLayout(frame_ImageArea);
        verticalLayout_9->setObjectName(QStringLiteral("verticalLayout_9"));
        verticalLayout_9->setContentsMargins(3, 3, 3, 3);
        labelImage = new QLabel(frame_ImageArea);
        labelImage->setObjectName(QStringLiteral("labelImage"));
        sizePolicy1.setHeightForWidth(labelImage->sizePolicy().hasHeightForWidth());
        labelImage->setSizePolicy(sizePolicy1);
        labelImage->setMinimumSize(QSize(200, 200));
        labelImage->setMaximumSize(QSize(200, 200));
        labelImage->setScaledContents(true);

        verticalLayout_9->addWidget(labelImage);


        verticalLayout->addWidget(frame_ImageArea);

        pbtnImage = new QPushButton(frame_ImageEditArea);
        pbtnImage->setObjectName(QStringLiteral("pbtnImage"));

        verticalLayout->addWidget(pbtnImage);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);


        horizontalLayout_3->addWidget(frame_ImageEditArea);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        verticalLayout_4->setSizeConstraint(QLayout::SetDefaultConstraint);
        verticalLayout_4->setContentsMargins(-1, 0, -1, -1);
        label_PDBid = new QLabel(groupBox_InformationRecord);
        label_PDBid->setObjectName(QStringLiteral("label_PDBid"));

        verticalLayout_4->addWidget(label_PDBid);

        lineEdit_PDBid = new QLineEdit(groupBox_InformationRecord);
        lineEdit_PDBid->setObjectName(QStringLiteral("lineEdit_PDBid"));

        verticalLayout_4->addWidget(lineEdit_PDBid);

        groupBox_ShortDescriptionArea = new QGroupBox(groupBox_InformationRecord);
        groupBox_ShortDescriptionArea->setObjectName(QStringLiteral("groupBox_ShortDescriptionArea"));
        groupBox_ShortDescriptionArea->setMaximumSize(QSize(16777215, 16777215));
        verticalLayout_3 = new QVBoxLayout(groupBox_ShortDescriptionArea);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(1, 1, 1, 1);
        plainTextEdit_ShortDescription = new QPlainTextEdit(groupBox_ShortDescriptionArea);
        plainTextEdit_ShortDescription->setObjectName(QStringLiteral("plainTextEdit_ShortDescription"));
        plainTextEdit_ShortDescription->setFrameShape(QFrame::NoFrame);

        verticalLayout_3->addWidget(plainTextEdit_ShortDescription);


        verticalLayout_4->addWidget(groupBox_ShortDescriptionArea);


        horizontalLayout_3->addLayout(verticalLayout_4);


        verticalLayout_5->addLayout(horizontalLayout_3);

        groupBox_ProtertiesArea = new QGroupBox(groupBox_InformationRecord);
        groupBox_ProtertiesArea->setObjectName(QStringLiteral("groupBox_ProtertiesArea"));
        verticalLayout_6 = new QVBoxLayout(groupBox_ProtertiesArea);
        verticalLayout_6->setObjectName(QStringLiteral("verticalLayout_6"));
        verticalLayout_6->setContentsMargins(1, 1, 1, 1);
        scrollArea_PropertiesArea = new QScrollArea(groupBox_ProtertiesArea);
        scrollArea_PropertiesArea->setObjectName(QStringLiteral("scrollArea_PropertiesArea"));
        scrollArea_PropertiesArea->setFrameShape(QFrame::NoFrame);
        scrollArea_PropertiesArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QStringLiteral("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 405, 264));
        verticalLayout_10 = new QVBoxLayout(scrollAreaWidgetContents);
        verticalLayout_10->setObjectName(QStringLiteral("verticalLayout_10"));
        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        label_Name = new QLabel(scrollAreaWidgetContents);
        label_Name->setObjectName(QStringLiteral("label_Name"));
        label_Name->setMinimumSize(QSize(100, 0));
        label_Name->setMaximumSize(QSize(100, 16777215));

        horizontalLayout_4->addWidget(label_Name);

        lineEdit_Name = new QLineEdit(scrollAreaWidgetContents);
        lineEdit_Name->setObjectName(QStringLiteral("lineEdit_Name"));

        horizontalLayout_4->addWidget(lineEdit_Name);


        verticalLayout_10->addLayout(horizontalLayout_4);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        label_Method = new QLabel(scrollAreaWidgetContents);
        label_Method->setObjectName(QStringLiteral("label_Method"));
        label_Method->setMinimumSize(QSize(100, 0));
        label_Method->setMaximumSize(QSize(100, 16777215));

        horizontalLayout_5->addWidget(label_Method);

        lineEdit_Method = new QLineEdit(scrollAreaWidgetContents);
        lineEdit_Method->setObjectName(QStringLiteral("lineEdit_Method"));

        horizontalLayout_5->addWidget(lineEdit_Method);


        verticalLayout_10->addLayout(horizontalLayout_5);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setObjectName(QStringLiteral("horizontalLayout_10"));
        label_Length = new QLabel(scrollAreaWidgetContents);
        label_Length->setObjectName(QStringLiteral("label_Length"));
        label_Length->setMinimumSize(QSize(100, 0));
        label_Length->setMaximumSize(QSize(100, 16777215));

        horizontalLayout_10->addWidget(label_Length);

        spinBox_Length = new QSpinBox(scrollAreaWidgetContents);
        spinBox_Length->setObjectName(QStringLiteral("spinBox_Length"));
        spinBox_Length->setMinimum(1);
        spinBox_Length->setMaximum(100000);
        spinBox_Length->setValue(1);

        horizontalLayout_10->addWidget(spinBox_Length);


        verticalLayout_10->addLayout(horizontalLayout_10);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        label_Authors = new QLabel(scrollAreaWidgetContents);
        label_Authors->setObjectName(QStringLiteral("label_Authors"));
        label_Authors->setMinimumSize(QSize(100, 0));
        label_Authors->setMaximumSize(QSize(100, 16777215));

        horizontalLayout_6->addWidget(label_Authors);

        lineEdit_Authors = new QLineEdit(scrollAreaWidgetContents);
        lineEdit_Authors->setObjectName(QStringLiteral("lineEdit_Authors"));

        horizontalLayout_6->addWidget(lineEdit_Authors);


        verticalLayout_10->addLayout(horizontalLayout_6);

        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setObjectName(QStringLiteral("horizontalLayout_11"));
        label_Resolution = new QLabel(scrollAreaWidgetContents);
        label_Resolution->setObjectName(QStringLiteral("label_Resolution"));
        label_Resolution->setMinimumSize(QSize(100, 0));
        label_Resolution->setMaximumSize(QSize(100, 16777215));

        horizontalLayout_11->addWidget(label_Resolution);

        doubleSpinBox_Resolution = new QDoubleSpinBox(scrollAreaWidgetContents);
        doubleSpinBox_Resolution->setObjectName(QStringLiteral("doubleSpinBox_Resolution"));
        doubleSpinBox_Resolution->setMinimum(0);
        doubleSpinBox_Resolution->setMaximum(30);
        doubleSpinBox_Resolution->setSingleStep(1);

        horizontalLayout_11->addWidget(doubleSpinBox_Resolution);


        verticalLayout_10->addLayout(horizontalLayout_11);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setObjectName(QStringLiteral("horizontalLayout_9"));
        label_ModelCount = new QLabel(scrollAreaWidgetContents);
        label_ModelCount->setObjectName(QStringLiteral("label_ModelCount"));
        label_ModelCount->setMinimumSize(QSize(100, 0));
        label_ModelCount->setMaximumSize(QSize(100, 16777215));

        horizontalLayout_9->addWidget(label_ModelCount);

        spinBox_ModelCount = new QSpinBox(scrollAreaWidgetContents);
        spinBox_ModelCount->setObjectName(QStringLiteral("spinBox_ModelCount"));
        spinBox_ModelCount->setMinimum(1);
        spinBox_ModelCount->setMaximum(10000);

        horizontalLayout_9->addWidget(spinBox_ModelCount);


        verticalLayout_10->addLayout(horizontalLayout_9);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_10->addItem(verticalSpacer_2);

        scrollArea_PropertiesArea->setWidget(scrollAreaWidgetContents);

        verticalLayout_6->addWidget(scrollArea_PropertiesArea);


        verticalLayout_5->addWidget(groupBox_ProtertiesArea);


        verticalLayout_7->addWidget(groupBox_InformationRecord);

        frame_ButtonsArea = new QFrame(centralwidget);
        frame_ButtonsArea->setObjectName(QStringLiteral("frame_ButtonsArea"));
        QSizePolicy sizePolicy2(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(frame_ButtonsArea->sizePolicy().hasHeightForWidth());
        frame_ButtonsArea->setSizePolicy(sizePolicy2);
        frame_ButtonsArea->setFrameShape(QFrame::NoFrame);
        frame_ButtonsArea->setFrameShadow(QFrame::Sunken);
        verticalLayout_2 = new QVBoxLayout(frame_ButtonsArea);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(5, 5, 5, 5);
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        pbtnDelete = new QPushButton(frame_ButtonsArea);
        pbtnDelete->setObjectName(QStringLiteral("pbtnDelete"));

        horizontalLayout->addWidget(pbtnDelete);

        pbtnAdd = new QPushButton(frame_ButtonsArea);
        pbtnAdd->setObjectName(QStringLiteral("pbtnAdd"));

        horizontalLayout->addWidget(pbtnAdd);

        pbtnEdit = new QPushButton(frame_ButtonsArea);
        pbtnEdit->setObjectName(QStringLiteral("pbtnEdit"));

        horizontalLayout->addWidget(pbtnEdit);


        verticalLayout_2->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        pbtnCancel = new QPushButton(frame_ButtonsArea);
        pbtnCancel->setObjectName(QStringLiteral("pbtnCancel"));

        horizontalLayout_2->addWidget(pbtnCancel);

        pbtnSave = new QPushButton(frame_ButtonsArea);
        pbtnSave->setObjectName(QStringLiteral("pbtnSave"));

        horizontalLayout_2->addWidget(pbtnSave);


        verticalLayout_2->addLayout(horizontalLayout_2);


        verticalLayout_7->addWidget(frame_ButtonsArea);


        horizontalLayout_7->addLayout(verticalLayout_7);

        PDB_MainWindow->setCentralWidget(centralwidget);
#ifndef QT_NO_SHORTCUT
        label_PDBid->setBuddy(lineEdit_PDBid);
        label_Name->setBuddy(lineEdit_Name);
        label_Method->setBuddy(lineEdit_Method);
        label_Length->setBuddy(spinBox_Length);
        label_Authors->setBuddy(lineEdit_Authors);
        label_Resolution->setBuddy(doubleSpinBox_Resolution);
        label_ModelCount->setBuddy(spinBox_ModelCount);
#endif // QT_NO_SHORTCUT

        retranslateUi(PDB_MainWindow);

        QMetaObject::connectSlotsByName(PDB_MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *PDB_MainWindow)
    {
        PDB_MainWindow->setWindowTitle(QApplication::translate("PDB_MainWindow", "Protein Data Base", 0));
        menuFile_Load->setText(QApplication::translate("PDB_MainWindow", "Load base", 0));
#ifndef QT_NO_TOOLTIP
        listWidget_RecordsList->setToolTip(QApplication::translate("PDB_MainWindow", "Double click to show", 0));
#endif // QT_NO_TOOLTIP
        pbtnCreateNew->setText(QApplication::translate("PDB_MainWindow", "Create &new record", 0));
        groupBox_InformationRecord->setTitle(QApplication::translate("PDB_MainWindow", "Information about selected record", 0));
#ifndef QT_NO_TOOLTIP
        labelImage->setToolTip(QApplication::translate("PDB_MainWindow", "default", 0));
#endif // QT_NO_TOOLTIP
        labelImage->setText(QString());
        labelImage->setProperty("imageSource", QVariant(QApplication::translate("PDB_MainWindow", "default", 0)));
        pbtnImage->setText(QApplication::translate("PDB_MainWindow", "pbtnImage", 0));
        label_PDBid->setText(QApplication::translate("PDB_MainWindow", "PDB &ID:", 0));
        groupBox_ShortDescriptionArea->setTitle(QApplication::translate("PDB_MainWindow", "Short &description", 0));
        plainTextEdit_ShortDescription->setPlainText(QString());
        plainTextEdit_ShortDescription->setPlaceholderText(QString());
        groupBox_ProtertiesArea->setTitle(QApplication::translate("PDB_MainWindow", "Properties:", 0));
        label_Name->setText(QApplication::translate("PDB_MainWindow", "Name", 0));
        label_Method->setText(QApplication::translate("PDB_MainWindow", "Method", 0));
        label_Length->setText(QApplication::translate("PDB_MainWindow", "Number aminoacids", 0));
        label_Authors->setText(QApplication::translate("PDB_MainWindow", "Authors", 0));
        label_Resolution->setText(QApplication::translate("PDB_MainWindow", "Resolution (A)", 0));
        label_ModelCount->setText(QApplication::translate("PDB_MainWindow", "Number models", 0));
        pbtnDelete->setText(QApplication::translate("PDB_MainWindow", "&Delete", 0));
        pbtnAdd->setText(QApplication::translate("PDB_MainWindow", "&Add as new", 0));
        pbtnEdit->setText(QApplication::translate("PDB_MainWindow", "&Edit", 0));
        pbtnCancel->setText(QApplication::translate("PDB_MainWindow", "&Cancel", 0));
#ifndef QT_NO_TOOLTIP
        pbtnSave->setToolTip(QString());
#endif // QT_NO_TOOLTIP
        pbtnSave->setText(QApplication::translate("PDB_MainWindow", "&Save", 0));
    } // retranslateUi

};

namespace Ui {
    class PDB_MainWindow: public Ui_PDB_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PDB_MAINWINDOW_H
