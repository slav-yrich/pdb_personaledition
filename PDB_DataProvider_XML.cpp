#include "PDB_DataProvider_XML.h"

PDB_DataProvider_XML::PDB_DataProvider_XML()
{
	domDoc = new QDomDocument();
}
PDB_DataProvider_XML::~PDB_DataProvider_XML()
{
	delete domDoc; //метод clear() в классе QDomDocument бросает исключение!
}


bool PDB_DataProvider_XML::DefineDataSource(const QString &sourceFileName)
{
	if (QFile::exists(sourceFileName))
	{
		fileName = sourceFileName;
		qDebug() << "Succes set file name source data";
		return true;
	}
	qDebug() << "Failure set file name source data";
	return false;
}

bool PDB_DataProvider_XML::LoadDataBase()
{
	bool check = false;
	if (!fileName.isNull())
	{
		QFile file(fileName);

		if (file.open(QIODevice::ReadOnly))
		{
			if (domDoc->setContent(&file))
			{
				check = true;
				qDebug() << "Base loaded successfully.";
			}
			else
			{
				qDebug() << "Error loading database. Could not set dom content.";
			}

			file.close();
			
		}
		else
		{
			qDebug() << "Error loading database. Could not open file.";
		}
	}
	else
	{
		qDebug() << "Error loading database. The name of source file  not set!";
	}
	return check;
} 

bool PDB_DataProvider_XML::CreateNewDataBase(const QString &newFileName)
{
	if (newFileName != "")
	{
		//Сначала проверяем возможность создания файла с указанным именем для новой базы
		QFile file(newFileName);
		if (file.open(QIODevice::WriteOnly)) 
		{
			file.close();

			domDoc = new QDomDocument("ProteinDataBase");
			domDoc->appendChild(getNewElement("ProteinDataBase", "", "", "", "", ""));
			
			DefineDataSource(newFileName);
			SaveDataBase();
			return true;
		}
	}
	return false;
}

bool PDB_DataProvider_XML::SaveDataBase()
{
	if (!domDoc->isNull())
	{
		QFile saveFile(fileName);
		if (saveFile.open(QFile::WriteOnly))
		{
			try
			{
				QTextStream out(&saveFile);
				domDoc->save(out, 2, QDomNode::EncodingFromDocument);
				saveFile.close();
				qDebug() << "DomDoc saved.";
				return true;
			}
			catch (const std::exception& exc)
			{
				qDebug() << "DomDoc not saved. Exception: "<< exc.what();
			}
		}
	}
	return false;
}

QList<QString> PDB_DataProvider_XML::GetRecordsList()
{
	QList<QString> pdbRecordsList = QList<QString>();
	if (!domDoc->isNull())
	{
		QDomNodeList nodeList = domDoc->elementsByTagName("Record");
		if (!nodeList.isEmpty())
		{
			for (int nodeIterator = 0; nodeIterator < nodeList.length(); nodeIterator++)
			{
				QDomElement domElement = nodeList.at(nodeIterator).toElement();
				if (!domElement.isNull())
				{
					pdbRecordsList.append(domElement.attribute("id", ""));
				}
			}
		}
	}
	return pdbRecordsList;
}

PDB_Record PDB_DataProvider_XML::GetSelectedRecord(const QString &PDB_ID) const
{
	PDB_Record selectedRecord = PDB_Record();
	QDomElement selectedDomElement; 
	if (FindSelectedRecord(selectedDomElement, PDB_ID))
	{
		selectedRecord.set_PDB_ID(selectedDomElement.attribute("id", ""));

		//Read description
		QDomElement domChildElement = selectedDomElement.firstChildElement("Description");
		if (!domChildElement.isNull()) selectedRecord.set_PDB_Description(domChildElement.text());
		
		//Read properties list
		QDomNodeList propertiesNodeList = selectedDomElement.elementsByTagName("Property");
		if (!propertiesNodeList.isEmpty())
		{
			for (int nodeIterator = 0; nodeIterator < propertiesNodeList.length(); nodeIterator++)
			{
				QDomElement domElementpdbProperty = propertiesNodeList.at(nodeIterator).toElement();
				if (!domElementpdbProperty.isNull())
				{
					QString pdbPropertyType = domElementpdbProperty.attribute("type", "");
					PDB_Property newpdbProperty = PDB_Property(PDB_Property::QStringTopdbProperty(pdbPropertyType));
					if (newpdbProperty.SetpdbPropertyName(domElementpdbProperty.attribute("name", "")))
					{
						newpdbProperty.SetpdbPropertyValue(domElementpdbProperty.text());
						selectedRecord.addpdbProperty(newpdbProperty);
					}
				}
			}
		}
	}
	else
	{
		qDebug() << "Element not find";
	}
	return selectedRecord;
}

bool PDB_DataProvider_XML::FindSelectedRecord(QDomElement &selectedDomElement, const QString &PDB_ID) const
{
	QDomElement domElement = QDomElement();
	if (!domDoc->isNull())
	{
		QDomNodeList nodeList = domDoc->elementsByTagName("Record");
		if (!nodeList.isEmpty())
		{
			for (int nodeIterator = 0; nodeIterator < nodeList.length(); nodeIterator++)
			{
				domElement = nodeList.at(nodeIterator).toElement();
				if (!domElement.isNull())
				{
					if ((domElement.tagName() == "Record") && (domElement.attribute("id", "") == PDB_ID))
					{
						//Надо учесть возможные повторения в файле, если кто-то его изменил вручную
						selectedDomElement = domElement;
						return true;
					}
				}
			}
		}
	}
	return false;
}

bool PDB_DataProvider_XML::SaveSelectedRecord(const QString &oldPDB_ID, const PDB_Record &record)
{
	QDomElement newElement = RecordToDomElement(record);
	QDomElement selectedDomElement;
	
	if (FindSelectedRecord(selectedDomElement, oldPDB_ID))
	{
		domDoc->documentElement().replaceChild(newElement, selectedDomElement);
		return SaveDataBase();
	}
	return false;
}

bool PDB_DataProvider_XML::DeleteSelectedRecord(const QString &PDB_ID)
{
	QDomElement selectedDomElement;
	if (FindSelectedRecord(selectedDomElement, PDB_ID))
	{
		domDoc->documentElement().removeChild(selectedDomElement);
		return SaveDataBase();
	}
	return false;
}

bool PDB_DataProvider_XML::AddRecord(const PDB_Record &record)
{  
	QDomElement domElement;
	//Проверим уникальность PDB ID добавляемой записи
	if (!FindSelectedRecord(domElement, record.get_PDB_ID()))
	{
		domElement = RecordToDomElement(record);
		domDoc->documentElement().appendChild(domElement);
		return SaveDataBase();
	}
	return false;
}

QDomElement PDB_DataProvider_XML::getNewElement(const QString &TagName, 
	const QString &Attribute1_Name, const QString &Attribute1_Value, 
	const QString &Attribute2_Name, const QString &Attribute2_Value,
	const QString &TextValue) const
{
	
	QDomElement domElement = QDomElement(); 
	if (!TagName.isEmpty())
	{
		domElement = domDoc->createElement(TagName);
		if (!Attribute1_Name.isEmpty())
		{
			QDomAttr domAttr1 = domDoc->createAttribute(Attribute1_Name);
			domAttr1.setValue(Attribute1_Value);
			domElement.setAttributeNode(domAttr1);
		}
		if (!Attribute2_Name.isEmpty())
		{
			QDomAttr domAttr2 = domDoc->createAttribute(Attribute2_Name);
			domAttr2.setValue(Attribute2_Value);
			domElement.setAttributeNode(domAttr2);
		}
		if (!TextValue.isEmpty()) 
		{
			QDomText domText = domDoc->createTextNode(TextValue);
			domElement.appendChild(domText);
		}
	}
	return domElement;
}

QDomElement PDB_DataProvider_XML::RecordToDomElement(const PDB_Record &record) const
{
	QDomElement domElement = getNewElement("Record","id", record.get_PDB_ID(),"", "","");
	domElement.appendChild(getNewElement("Description", "","","","", record.get_PDB_Description()));
	QDomElement domElementProperties= getNewElement("Properties", "", "","","","");
	for (int pdbPropertyIterator = 0; pdbPropertyIterator < record.pdbPropertyListCount(); pdbPropertyIterator++)
	{
		PDB_Property pdbProperty = record.getpdbProperty(pdbPropertyIterator);
		domElementProperties.appendChild
			(
				getNewElement
				(
					"Property",
					"type", PDB_Property::pdbPropertyToQString(pdbProperty.GetpdbPropertyType()),
					"name", pdbProperty.GetpdbPropertyName(),
					pdbProperty.GetpdbPropertyValue()
					)
				);
	}
	domElement.appendChild(domElementProperties);
	return domElement;
}