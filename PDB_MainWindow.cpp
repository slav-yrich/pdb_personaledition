#include <PDB_MainWindow.h>
//public methods:
PDB_MainWindow::PDB_MainWindow(QWidget *parrentQWidget):QMainWindow(parrentQWidget)
{
	setupUi(this);
	dataProvider=new PDB_DataProvider_XML();
	//Отмена (выход из любого режима)
	QObject::connect(pbtnCancel, SIGNAL(clicked()), this, SLOT(slotInitialStateMode()));
	//Режим редактирования
	QObject::connect(pbtnEdit, SIGNAL(clicked()), this, SLOT(slotEditMode()));
	//Режим создания новой записи
	QObject::connect(pbtnCreateNew, SIGNAL(clicked()), this, SLOT(slotCreateNewRecordMode()));
	//Показ выбранной записи
	QObject::connect(listWidget_RecordsList, SIGNAL(itemDoubleClicked(QListWidgetItem*)), this, SLOT(slotShowSelectedRecord(QListWidgetItem*)));
	//Сохранение выбранной записи
	QObject::connect(pbtnSave, SIGNAL(clicked()), this, SLOT(slotSaveSelectedRecord()));
	//Добавление записи
	QObject::connect(pbtnAdd, SIGNAL(clicked()), this, SLOT(slotAddRecord()));
	//Удаление записи
	QObject::connect(pbtnDelete, SIGNAL(clicked()), this, SLOT(slotDeleteRecord()));
	//Редактирование картинки
	QObject::connect(pbtnImage, SIGNAL(clicked()), this, SLOT(slotEditRecordPicture()));
	//Проверка ввода в поле ShortDescription
	QObject::connect(plainTextEdit_ShortDescription,SIGNAL(textChanged()), this, SLOT(slotCheckShortDescription()));

	SetValidatorsFields();
	slotLoadDataBase();

	
}

PDB_MainWindow::~PDB_MainWindow()
{
	delete dataProvider;
}

void PDB_MainWindow::ReadPDBRecordsList()
{
	QList<QString> pdbRecordsList = dataProvider->GetRecordsList();
	if (pdbRecordsList.count() > 0)
	{
		listWidget_RecordsList->clear();
		for (int pdbRecordIterator = 0; pdbRecordIterator < pdbRecordsList.count(); pdbRecordIterator++)
		{
			listWidget_RecordsList->addItem(pdbRecordsList.at(pdbRecordIterator));
		}
	}	
}
//private methods:
void PDB_MainWindow::SetReadOnlyEditFields(const bool &value)
{
	lineEdit_PDBid->setReadOnly(value);
	plainTextEdit_ShortDescription->setReadOnly(value);
	lineEdit_Name->setReadOnly(value);
	lineEdit_Method->setReadOnly(value);
	spinBox_Length->setReadOnly(value);
	lineEdit_Authors->setReadOnly(value);
	doubleSpinBox_Resolution->setReadOnly(value);
	spinBox_ModelCount->setReadOnly(value);
}

void PDB_MainWindow::SetValidatorsFields()
{
	lineEdit_PDBid->setValidator(new QRegExpValidator(PDB_Property::GetQRegExpForText(), this));
	lineEdit_Name->setValidator(new QRegExpValidator(PDB_Property::GetQRegExpForText(), this));
	lineEdit_Method->setValidator(new QRegExpValidator(PDB_Property::GetQRegExpForText(), this));
	lineEdit_Authors->setValidator(new QRegExpValidator(PDB_Property::GetQRegExpForText(), this));

	//Подсказки о валидации:
	QString toolTipCommonString = "You can enter numbers, Latin letters and symbols .,;:!?\"\'() and space.";
	lineEdit_PDBid->setToolTip("PDB ID must be unique. "+toolTipCommonString);
	plainTextEdit_ShortDescription->setToolTip(toolTipCommonString);
	lineEdit_Name->setToolTip(toolTipCommonString);
	lineEdit_Method->setToolTip(toolTipCommonString);
	lineEdit_Authors->setToolTip(toolTipCommonString);

	spinBox_Length->setToolTip("You can enter an integer of "+ QString::number(spinBox_Length->minimum()) + " to " + QString::number(spinBox_Length->maximum()) + ".");
	spinBox_ModelCount->setToolTip("You can enter an integer of " + QString::number(spinBox_ModelCount->minimum()) + " to " + QString::number(spinBox_ModelCount->maximum()) + ".");
	doubleSpinBox_Resolution->setToolTip("You can enter an real number of " + QString::number(doubleSpinBox_Resolution->minimum()) + " to " + QString::number(doubleSpinBox_Resolution->maximum()) + ".");
}

void PDB_MainWindow::ClearEditFields()
{
	SetDefaultPicture();

	lineEdit_PDBid->clear();
	plainTextEdit_ShortDescription->clear();

	lineEdit_Name->clear();
	lineEdit_Method->clear();
	lineEdit_Authors->clear();

	spinBox_Length->setValue(0);
	doubleSpinBox_Resolution->setValue(0.0);
	spinBox_ModelCount->setValue(0);

	spinBox_Length->clear();
	doubleSpinBox_Resolution->clear();
	spinBox_ModelCount->clear();
}

void PDB_MainWindow::SetDefaultPicture()
{
	labelImage->clear();
	pbtnImage->setText("Add &picture");
	labelImage->setToolTip("default");

	QPixmap pixMap;
	if (pixMap.load("defaultImage.png"))
	{
		labelImage->setPixmap(pixMap);
	}
	else
	{
		if (RhetoricalQuestionCounter < 3)
		{
			QMessageBox::question(this, "Rhetorical question", "Why did you removed the default picture???", QMessageBox::Ok);
			RhetoricalQuestionCounter++;
		}
	}
}

bool PDB_MainWindow::SetPicture(const QString &picturePath)
{
	QPixmap pixMap;
	if (pixMap.load(QFileInfo(picturePath).filePath())) //Может это и лишнее, если не не поможет с направлением наклона слеша
	{
		labelImage->setPixmap(pixMap);
		labelImage->setToolTip(picturePath);
		pbtnImage->setText("Change &picture");
		return true;
	}
	return false;
	
}

PDB_Record PDB_MainWindow::AllFieldsToRecord(bool &check) const
{
	check = false;
	PDB_Record resultRecord = PDB_Record();
	if ((plainTextEdit_ShortDescription->toPlainText() == "")
		|| (((plainTextEdit_ShortDescription->toPlainText() != ""))&&(resultRecord.set_PDB_Description(plainTextEdit_ShortDescription->toPlainText()))))
	{
		
		try
		{
			if (labelImage->toolTip() != "default")
				resultRecord.addpdbProperty(Picture, PDB_Property::pdbPropertyToQString(Picture), labelImage->toolTip());
			if (label_Name->text() != "")
				resultRecord.addpdbProperty(Text, label_Name->text(), lineEdit_Name->text());
			if (label_Method->text() != "")
				resultRecord.addpdbProperty(Text, label_Method->text(), lineEdit_Method->text());
			if (label_Authors->text() != "")
				resultRecord.addpdbProperty(Text, label_Authors->text(), lineEdit_Authors->text());
			if (spinBox_Length->value() != 0)
				resultRecord.addpdbProperty(IntegerNumber, label_Length->text(), QString::number(spinBox_Length->value()));
			if (spinBox_ModelCount->value() != 0)
				resultRecord.addpdbProperty(IntegerNumber, label_ModelCount->text(), QString::number(spinBox_ModelCount->value()));
			if (doubleSpinBox_Resolution->value() != 0)
				resultRecord.addpdbProperty(RealNumber, label_Resolution->text(), QString::number(doubleSpinBox_Resolution->value()));
			check = true;
		}
		catch (const std::exception& exc)
		{
			QMessageBox::critical(0, QString("Error"), QString("Input incorrect value property!"));
		}
	}
	else
	{
		QMessageBox::critical(0, QString("Error"), QString("Input incorrect value short descryption!"));
	}
	return resultRecord;

}

//private slots

void PDB_MainWindow::slotLoadDataBase()
{
	if (!dataProvider->DefineDataSource(PDB_DataSource_FileName))
	{
		//Если не удалось задать источник данных, то это значит, что файла базы ещё нет или он удалён/переименован.
		//Значит создадим новый (Проверка успешности не нужна, так как это проверится при загрузке)
		dataProvider->CreateNewDataBase(PDB_DataSource_FileName);
	}

	if (dataProvider->LoadDataBase())
	{
		ReadPDBRecordsList();
		slotInitialStateMode();
	}
	else //Если произошла ошибка при чтении базы данных
	{
		switch (QMessageBox::critical(this, "Error", "Error loading database! Retry loading? Or create a new database?",
			QMessageBox::Retry | QMessageBox::Yes | QMessageBox::No))
		{
		case QMessageBox::Retry: //Выбор повторной загрузки
		{
			slotLoadDataBase();
			break;
		}
		case QMessageBox::Yes: //Выбор создания новой
		{
			if (dataProvider->CreateNewDataBase(PDB_DataSource_FileName))
			{
				slotLoadDataBase();
			}
			break;
		}
		default: //Выбор "No" или закрытие диалогового окна
		{
			QWidget::setAttribute(Qt::WA_DeleteOnClose, true);
			close();
			//break; //только зачем он здесь? 
		}
		}
	}
}

void PDB_MainWindow::slotShowSelectedRecord(QListWidgetItem *chooseItem)
{
	qDebug() << "ChooseItem: " << chooseItem->text();
	PDB_Record chooseItemPDBRecord = dataProvider->GetSelectedRecord(chooseItem->text());
	if (chooseItemPDBRecord.get_PDB_ID() != "")
	{
		slotInitialStateMode();

		lineEdit_PDBid->setText(chooseItemPDBRecord.get_PDB_ID());
		plainTextEdit_ShortDescription->setPlainText(chooseItemPDBRecord.get_PDB_Description());
		for (int pdbPropertyIterator = 0; pdbPropertyIterator < chooseItemPDBRecord.pdbPropertyListCount(); pdbPropertyIterator++)
		{
			//временный код для поддержки старого формата gui
			PDB_Property pdbProperty = chooseItemPDBRecord.getpdbProperty(pdbPropertyIterator);
			switch (pdbProperty.GetpdbPropertyType())
			{
			case Text:
				if (pdbProperty.GetpdbPropertyName() == label_Name->text())
				{
					lineEdit_Name->setText(pdbProperty.GetpdbPropertyValue());
				}
				if (pdbProperty.GetpdbPropertyName() == label_Method->text())
				{
					lineEdit_Method->setText(pdbProperty.GetpdbPropertyValue());
				}
				if (pdbProperty.GetpdbPropertyName() == label_Authors->text())
				{
					lineEdit_Authors->setText(pdbProperty.GetpdbPropertyValue());
				}
				break;
			case IntegerNumber:
				if (pdbProperty.GetpdbPropertyName() == label_Length->text())
				{
					spinBox_Length->setValue((pdbProperty.GetpdbPropertyValue()).toInt());
				}
				if (pdbProperty.GetpdbPropertyName() == label_ModelCount->text())
				{
					spinBox_ModelCount->setValue((pdbProperty.GetpdbPropertyValue()).toInt());
				}
				break;
			case RealNumber:
				if (pdbProperty.GetpdbPropertyName() == label_Resolution->text())
				{
					doubleSpinBox_Resolution->setValue((pdbProperty.GetpdbPropertyValue()).toDouble());
				}
				break;
			case Picture:
				if (pdbProperty.GetpdbPropertyValue() == "default")
				{
					SetDefaultPicture();
				}
				else
				{
					if (!SetPicture(pdbProperty.GetpdbPropertyValue()))
					{
						QMessageBox::critical(this, "Error", "Error loading picture file.");
						SetDefaultPicture();
					}
				}
				break;
			default:
				break;
			}
		}

		SetReadOnlyEditFields(true);
		frame_ButtonsArea->show();
	}
}


void PDB_MainWindow::slotSaveSelectedRecord()
{
	qDebug() << "slotSaveSelectedRecord";
	PDB_Record newRecord = PDB_Record();
	bool check = true;
	newRecord = AllFieldsToRecord(check);
	if (check)
	{
		qDebug() << "slotSaveSelectedRecord";
		if (lineEdit_PDBid->text() == listWidget_RecordsList->currentItem()->text())
		{
			//Если PDB ID не изменился
			check &= newRecord.set_PDB_ID(lineEdit_PDBid->text());
			if (!check) QMessageBox::critical(0, QString("Error"), QString("Input incorrect value PDB ID!"));
		}
		else
		{
			//Если PDB ID изменился
			check &= newRecord.set_PDB_ID(lineEdit_PDBid->text(), dataProvider->GetRecordsList());
			if (!check) QMessageBox::critical(0, QString("Error"), QString("PDB ID already used or input incorrect value PDB ID!"));
		}
		if (check)
		{
			if (dataProvider->SaveSelectedRecord(listWidget_RecordsList->currentItem()->text(), newRecord))
			{
				slotInitialStateMode();
				ReadPDBRecordsList();
				qDebug() << "Selected element saved.";
				QMessageBox::information(this, "Report", " Record successfully saved");
			}
			else
			{
				QMessageBox::critical(this, "Error", " Record not save in data base!");
			}
		}
	}
}

void PDB_MainWindow::slotAddRecord()
{
	qDebug() << "slotAddRecord";
	PDB_Record newRecord = PDB_Record();
	bool check = true;
	newRecord = AllFieldsToRecord(check);
	if (check)
	{
		check &= newRecord.set_PDB_ID(lineEdit_PDBid->text(), dataProvider->GetRecordsList());
		if (!check)
		{
			QMessageBox::critical(0, QString("Error"), QString("PDB ID already used or input incorrect value PDB ID!"));
		}
		else
		{
			if (dataProvider->AddRecord(newRecord))
			{
				slotInitialStateMode();
				ReadPDBRecordsList();
				qDebug() << "DomNode add in domDoc";
				QMessageBox::information(this, "Report", " Record successfully added");
			}
			else
			{
				QMessageBox::critical(this, "Error", " Record not add in data base!");
			}
		}
	}
}

void PDB_MainWindow::slotDeleteRecord()
{
	if (dataProvider->DeleteSelectedRecord(lineEdit_PDBid->text()))
	{
		slotInitialStateMode();
		ReadPDBRecordsList();
		qDebug() << "DomNode delete from domDoc";
	}
	else
	{
		qDebug() << "DomNode NOT delete from domDoc";
	}
}

void PDB_MainWindow::slotCreateNewRecordMode()
{
	SetReadOnlyEditFields(false);
	ClearEditFields();

	pbtnEdit->hide();
	pbtnDelete->hide();
	pbtnSave->hide();

	frame_ButtonsArea->show();
	pbtnAdd->show();
	pbtnCancel->show();
	pbtnImage->show();

	SetDefaultPicture();

	qDebug() << "\"Create New Record\" mode activated.";
}

void PDB_MainWindow::slotEditMode()
{
	SetReadOnlyEditFields(false);

	pbtnEdit->hide();

	pbtnDelete->show();
	pbtnAdd->show();
	pbtnSave->show();
	pbtnCancel->show();
	pbtnImage->show();

	qDebug() << "\"Edit\" mode activated.";
}

void PDB_MainWindow::slotInitialStateMode()
{
	SetReadOnlyEditFields(true);
	ClearEditFields();

	frame_ButtonsArea->hide();
	pbtnImage->hide();
	pbtnSave->hide();
	pbtnAdd->hide();
	pbtnCancel->hide();

	pbtnDelete->show();
	pbtnEdit->show();

	SetDefaultPicture();

	qDebug() << "\"Initial State\" mode activated.";
}

void PDB_MainWindow::slotEditRecordPicture()
{
	QString typeDialog = "change";
	if (labelImage->toolTip() == "default") typeDialog = "add";
	//setDirectory 
	//QDir().mkdir("images");
	//QDir imageDir = QDir("images");
	QString picturePath = QFileDialog::getOpenFileName(this, "Dialog window " + typeDialog + "ing an image.", /*imageDir.absolutePath()*/"", "*.jpg *.png *.bmp");
	if (!picturePath.isEmpty())
	{
		//Наверняка это костыль, но будем копировать все изображения в корневую папку программы  
		//и записывать относительные пути, что бы можно было переносить базу данных.
		if (!QFile::exists(/*"images\\" + */QFileInfo(picturePath).fileName()))
		{
			if (QFile::copy(picturePath,/* "images\\" +*/ QFileInfo(picturePath).fileName()))
			{
				picturePath =/* "images\\" +*/ QFileInfo(picturePath).fileName();
			}
		}
		else
		{
			picturePath =/* "images\\" +*/ QFileInfo(picturePath).fileName();
		}
		if (SetPicture(picturePath))
		{
			QMessageBox::information(this, "Report", "Image successfully " + typeDialog + "ed.");
		}
		else
		{
			QMessageBox::critical(this, "Error", "Error loading file. The picture not " + typeDialog + "ed.");
		}
	}
}

void PDB_MainWindow::slotCheckShortDescription()
{
	qDebug() << "plainTextEdit_ShortDescription text changed!";
	if ((plainTextEdit_ShortDescription->toPlainText()=="")||(PDB_Property::CheckText(plainTextEdit_ShortDescription->toPlainText())))
	{
		plainTextEdit_ShortDescription->setStyleSheet("background-color: rgb(255, 255, 255)");
	}
	else
	{
		plainTextEdit_ShortDescription->setStyleSheet("background-color: rgb(255,0,0)");
	}
}