#include "PDB_Record.h"

PDB_Record::PDB_Record():PDB_ID(""), PDB_Description("")
{
	pdbPropertyList = new QList<PDB_Property>();
}


PDB_Record::~PDB_Record()
{
	//delete propertiesList;
}


bool PDB_Record::addpdbProperty(const PDB_PropertyType &pdbPropertyType, const QString &pdbPropertyName, const QString &pdbPropertyValue)
{
	bool check = false;
	PDB_Property pdbProperty = PDB_Property(pdbPropertyType);
	if (pdbProperty.SetpdbPropertyName(pdbPropertyName))
	{
		if (pdbProperty.SetpdbPropertyValue(pdbPropertyValue))
		{
			pdbPropertyList->append(pdbProperty);
			check = true;
		}
	}
	return check;
}

void PDB_Record::addpdbProperty(const PDB_Property &pdbProperty)
{
	pdbPropertyList->append(pdbProperty);
}

PDB_Property PDB_Record::getpdbProperty(const unsigned int &number) const
{
	if (number < pdbPropertyList->count())
	{
		return pdbPropertyList->at(number);
	}
	else
	{
		return PDB_Property(Text);
	}
}

int PDB_Record::pdbPropertyListCount() const
{
	return pdbPropertyList->count();
}

//PDB_Description;
const QString& PDB_Record::get_PDB_Description() const
{
	return PDB_Description;
}
bool PDB_Record::set_PDB_Description(const QString &Value)
{
	if (PDB_Property::CheckText(Value))
	{
		PDB_Description = Value;
		return true;
	}
	return false;
}

//PDB_ID
const QString& PDB_Record::get_PDB_ID() const
{
	return PDB_ID;
}

bool PDB_Record::set_PDB_ID(const QString &value)
{
	if ((value != "") && (PDB_Property::CheckText(value)))
	{
		PDB_ID = value;
		return true;
	}
	return false;
}

bool PDB_Record::set_PDB_ID(const QString &value, const QList<QString> &pdbRecordsList)
{
	if ((value != "") && (PDB_Property::CheckText(value)) && (pdbRecordsList.indexOf(value) < 0))
	{
		PDB_ID = value;
		return true;
	}
	return false;
}
